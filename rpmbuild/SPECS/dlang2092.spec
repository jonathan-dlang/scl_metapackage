# Define SCL name
%global vendor            jonathan
%global _scl_prefix       /opt/%{vendor}
%global scl_name_prefix   %{vendor}-
%global scl_name_base     dlang
%global scl_name_version  2092
%global scl               %{scl_name_prefix}%{scl_name_base}%{scl_name_version}


# Define SCL macros
# Support SCL over NFS.
%global nfsmountable      1
%{!?install_scl: %global install_scl 1}
%scl_package              %scl

# do not produce empty debuginfo package
%global debug_package %{nil}

Name:            %scl_name
Version:         1.0.0
Release:         1%{?dist}
Summary:         Package that installs %scl

License:         CECCIL
Source0:         LICENSE

Requires:        %{?scl_name}-runtime%{?_isa} = %{version}-%{release}

BuildRequires:   scl-utils-build
BuildRequires:   help2man


%description
This is the main package for %{scl} Software Collection, which installs
necessary packages to use %{scl_name_base} package.

%package         runtime
Summary:         Package that handles %scl Software Collection.
Requires:        scl-utils
Provides:        %{?scl_name}-runtime = %{version}-%{release}
Provides:        %{?scl_name}-runtime%{?_isa} = %{version}-%{release}

%description     runtime
Package shipping essential scripts to work with %scl Software Collection.


%package         build
Summary:         Package shipping basic build configuration
Requires:        scl-utils-build
Requires:        %{?scl_name}-runtime%{?_isa} = %{version}-%{release}

%description     build
Package shipping essential configuration macros to build %{scl} Software
Collection or packages depending on %{scl} Software Collection.

%package         scldevel
Summary:         Package shipping development files for %scl
Requires:        %{?scl_name}-runtime%{?_isa} = %{version}-%{release}

%description     scldevel
Package shipping development files, especially usefull for development of
packages depending on %scl Software Collection.

%package         syspaths
Summary:         System-wide wrappers for the %{pkg_name} package
Requires:        %{?scl_name}-runtime%{?_isa} = %{version}-%{release}


%description syspaths
System-wide wrappers for the %{pkg_name} package.

Using the %{pkg_name}-syspaths package does not require running the
'scl enable' or 'module command. This package practically replaces the system
default llvm-ldc package. It provides the llvm librairy mirror from ldc team.

Note that the llvm-ldc package conflict and cannot be installed on one system.

%prep
%autosetup -c -T
install -m 644 %{SOURCE0} ./
# This section generates README file from a template and creates man page
# from that file, expanding RPM macros in the template file
cat <<'EOF'> README
Collection.  For more information about Software Collections, see
scl(1). 

Usage: scl enable %{scl} '%{scl_name_base}'

Software Collections allows use of applications which are not located
in the filesystem root hierarchy but are present in an alternative
location, which is %{_scl_root} in case of the %{scl_name}
collection.


When working with %{scl_name} collection, use the "scl" utility (see
scl(1) for usage) to enable the scl environment properly.

Configuration for the %{scl_name} software collection is located under %{_sysconfdir}.

Examples:
scl enable %{scl_name} bash
  Run interactive shell where %{scl_name} software collection is enabled.

scl enable %{scl_name} 'man %{scl_name_base}'
  Show man pages for %{scl_name_base} command, which is part of the %{scl_name} software
  collection.
EOF

# generate a helper script that will be used by help2man
cat >h2m_helper <<'EOF'
#!/bin/bash
[[ "$1" == "--version" ]] && echo "%{scl_name} %{version} Software Collection" || cat README
EOF
chmod a+x h2m_helper

# create enable scriptlet that sets correct environment for collection
cat << EOF | tee enable
# For binaries
export PATH="%{_bindir}\${PATH:+:\${PATH}}"
# For libraries
export LD_LIBRARY_PATH="%{_libdir}\${LD_LIBRARY_PATH:+:\${LD_LIBRARY_PATH}}"
# For man pages; empty field makes man to consider also standard path
export MANPATH="%{_mandir}:\${MANPATH}"
export PKG_CONFIG_PATH="%{_libdir}/pkgconfig\${PKG_CONFIG_PATH:+:\${PKG_CONFIG_PATH}}"
EOF

# Broken: /usr/share/Modules/bin/createmodule.sh enable | tee envmod
# See https://bugzilla.redhat.com/show_bug.cgi?id=1197321
cat << EOF | tee envmod
#%%Module1.0
prepend-path    X_SCLS              %{scl}
prepend-path    PATH                %{_bindir}:%{_sbindir}
prepend-path    LD_LIBRARY_PATH     %{_libdir}
prepend-path    MANPATH             %{_mandir}
EOF

# generate rpm macros file for depended collections
cat << EOF | tee scldevel
%%scl_%{scl_name_base}        %{scl}
%%scl_prefix_%{scl_name_base} %{?scl_prefix}
EOF

# create directory for SCL register scripts
cat <<EOF | tee register
#!/bin/bash
for file in  %{?_scl_scripts}/register.d/*; do
    [[ -x \$f ]] && source \$(readlink -f \$file)
done
EOF

# and deregister as well
cat <<EOF | tee deregister
#!/bin/bash
for file in %{?_scl_scripts}/deregister.d/*; do
    [[ -x \$f ]] && source \$(readlink -f \$file)
done
EOF

# SELinux
cat <<EOF | tee selinux-set
#!/bin/sh
semanage fcontext -a -e / %{?_scl_root} >/dev/null 2>&1 || :
semanage fcontext -a -e %{_root_sysconfdir} %{_sysconfdir} >/dev/null 2>&1 || :
semanage fcontext -a -e %{_root_localstatedir} %{_localstatedir} >/dev/null 2>&1 || :
selinuxenabled && load_policy || :
EOF

cat <<EOF | tee selinux-restore
#!/bin/sh
restorecon -R %{?_scl_root} >/dev/null 2>&1 || :
restorecon -R %{_sysconfdir} >/dev/null 2>&1 || :
restorecon -R %{_localstatedir} >/dev/null 2>&1 || :
EOF

# syspath
cat <<EOF | tee %{scl_name_base}_wrapper
#!/bin/bash
source scl_source enable %{scl}
exec "%{_bindir}/%{scl_name_base}%{version_major}" "\$@"
EOF

%build
# generate the man page
help2man -N --section 7 ./h2m_helper -o %{scl_name}.7


%install

%{scl_install}

# Move in correct location, if needed
#if [[ "%%{_root_sysconfdir}/rpm" != "%%{macrosdir}" ]]; then
#  mv  %%{buildroot}%%{_root_sysconfdir}/rpm/macros.%%{scl}-config \
#      %%{buildroot}%%{macrosdir}/macros.%%{scl}-config
#fi

install -D -m 644 enable          %{buildroot}%{_scl_scripts}/enable
install -D -m 644 register        %{buildroot}%{_scl_scripts}/register
install -D -m 644 deregister      %{buildroot}%{_scl_scripts}/deregister
install -D -m 644 envmod          %{buildroot}%{_root_datadir}/Modules/modulefiles/%{scl_name}
install -D -m 644 scldevel        %{buildroot}%{_root_sysconfdir}/rpm/macros.%{scl_name_base}-scldevel
install -D -m 644 %{scl_name}.7   %{buildroot}%{_mandir}/man7/%{scl_name}.7
install -D -m 644 selinux-set     %{buildroot}%{_scl_scripts}/register.d/30.selinux-set
install -D -m 644 selinux-restore %{buildroot}%{_scl_scripts}/register.d/70.selinux-restore

# we need to own all these directories, so create them to have them listed
mkdir -p %{buildroot}%{?_scl_scripts}/register.content%{_unitdir}
mkdir -p %{buildroot}%{?_scl_scripts}/register.content%{_sysconfdir}
mkdir -p %{buildroot}%{?_scl_scripts}/register.d
mkdir -p %{buildroot}%{?_scl_scripts}/deregister.d

# syspaths
mkdir -p %{buildroot}%{_root_sysconfdir}
install -D -m 755 %{scl_name_base}_wrapper %{buildroot}%{_root_bindir}/%{scl_name_prefix}%{scl_name_base}%{scl_name_version}


%post runtime
# Simple copy of context from system root to SCL root.
# In case new version needs some additional rules or context definition,
# it needs to be solved in base system.
# semanage does not have -e option in RHEL-5, so we would
# have to have its own policy for collection.
%{?_scl_scripts}/register.d/30.selinux-set
%{?_scl_scripts}/register.d/70.selinux-restore


%files

%files runtime -f filelist
%license LICENSE
%doc README
%scl_files
%attr(0755,root,root) %{?_scl_scripts}/register
%attr(0755,root,root) %{?_scl_scripts}/deregister
%{?_scl_scripts}/register.content
%dir %{?_scl_scripts}/register.d
%dir %{?_scl_scripts}/deregister.d
%attr(0755,root,root) %{?_scl_scripts}/register.d/*
%{_root_datadir}/Modules/modulefiles/%{scl_name}

%files build
%{_root_sysconfdir}/rpm/macros.%{scl}-config

%files scldevel
%{_root_sysconfdir}/rpm/macros.%{scl_name_base}-scldevel

%files syspaths
%{_root_bindir}/%{scl_name_prefix}%{scl_name_base}%{scl_name_version}

%changelog
* Wed Feb 27 2019 Mercier Jonathan <jonathan.mercier@cng.fr> 1.0.0-1
- initial software collection

