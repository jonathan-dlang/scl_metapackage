#!/bin/bash
IMAGE="${1:-fedora:${FEDORA_VERSION}}"
CONTAINER_SCRIPTS_PATH="/opt/${VENDOR}/${VENDOR}-${SCL_COLLECTION}${DLANG_VERSION}/"
container=$(buildah from "${IMAGE}")

buildah run "${container}"  dnf install -y scl-utils bash-completion

buildah copy "${container}" \
    "rpmbuild/RPMS/x86_64/jonathan-dlang${DLANG_VERSION}-${METAPACKAGE_VERSION}-${SPEC_VERSION}.fc${FEDORA_VERSION}.x86_64.rpm" \
    rpmbuild/RPMS/x86_64/

buildah copy "${container}" \
    "rpmbuild/RPMS/x86_64/jonathan-dlang${DLANG_VERSION}-build-${METAPACKAGE_VERSION}-${SPEC_VERSION}.fc${FEDORA_VERSION}.x86_64.rpm" \
    rpmbuild/RPMS/x86_64/

buildah copy "${container}" \
    "rpmbuild/RPMS/x86_64/jonathan-dlang${DLANG_VERSION}-runtime-${METAPACKAGE_VERSION}-${SPEC_VERSION}.fc${FEDORA_VERSION}.x86_64.rpm" \
    rpmbuild/RPMS/x86_64/

buildah copy "${container}" \
    "rpmbuild/RPMS/x86_64/jonathan-dlang${DLANG_VERSION}-scldevel-${METAPACKAGE_VERSION}-${SPEC_VERSION}.fc${FEDORA_VERSION}.x86_64.rpm" \
    rpmbuild/RPMS/x86_64/

buildah run "${container}"  /bin/sh << EOF
dnf install -y rpmbuild/RPMS/x86_64/*.rpm

dnf clean all
rm -fr /var/cache/*
EOF
buildah config --author 'Jonathan MERCIER aka bioinfornatics' "${container}"
buildah config --comment 'Minimal Image to build dlang SCLs' "${container}"
buildah config --env CONTAINER_SCRIPTS_PATH="${CONTAINER_SCRIPTS_PATH}" "${container}"

echo "${container}"
